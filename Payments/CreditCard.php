<?php namespace App\Payments;

use App\Payments\Exceptions\BlankValueException;
use App\Payments\Exceptions\CreditCardNumberLengthException;
use \DateTime;

use App\Supplemental\Traits\EnhancedStudlyCase;

class CreditCard{

	use EnhancedStudlyCase;

	//Data holder class to pass around credit card information
	
	private $firstName;
	private $lastName;
	private $number;
	private $expirationDate;
	private $code;

	private $address;
	private $state;
	private $zip;

	const FIRST_NAME = 0;
	const LAST_NAME = 1;
	const NUMBER = 2;
	const EXPIRATION = 3;
	const CODE = 4;
	const ADDRESS = 5;
	const STATE = 6;
	const ZIP = 7;

	public function setFirstName($name){
		if(empty($name)){
			throw new BlankValueException('First Name');
		}

		$this->firstName = $this->eachToUpper($name);

		return $this;
	}

	public function setLastName($name){
		if(empty($name)){
			throw new BlankValueException('Last Name');
		}

		$this->lastName = $this->eachToUpper($name);

		return $this;
	}

	public function setNumber($number){
		if(strlen($number) != 16){
			throw new CreditCardNumberLengthException();
		}

		$this->number = $number;

		return $this;
	}

	public function setExpirationDate(DateTime $expirationDate){
		$this->expirationDate = $expirationDate;

		return $this;
	}

	public function setCode($code){
		if(empty($code)){
			throw new BlankValueException('Code');
		}

		$this->code = $code;

		return $this;
	}

	public function setAddress($address){
		if(empty($address)){
			throw new BlankValueException('Address');
		}

		$this->address = $this->eachToUpper($address);

		return $this;
	}

	public function setState($state){
		if(empty($state)){
			throw new BlankValueException('State');
		}

		$this->state = $state;

		return $this;
	}

	public function setZip($zip){
		if(empty($zip)){
			throw new BlankValueException('Zip');
		}

		$this->zip = $zip;

		return $this;
	}

	public function getFirstName(){
		return $this->firstName;
	}

	public function getLastName(){
		return $this->lastName;
	}

	public function getNumber(){
		return $this->number;
	}

	public function getExpirationDate(){
		if(!is_null($this->expirationDate)){
			return $this->expirationDate->format('my');
		}else{
			return null;
		}
	}

	public function getCode(){
		return $this->code;
	}

	public function getAddress(){
		return $this->address;
	}

	public function getState(){
		return $this->state;
	}

	public function getZip(){
		return $this->zip;
	}
}

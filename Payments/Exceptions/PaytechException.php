<?php namespace App\Payments\Exceptions;

use Exception;

abstract class PaytechException extends Exception {

	protected $modedErrorMsgs = [
		1 => "Your card has been declined. Please contact your card issuer.",
		2 => "Your card has been declined. Please contact your card issuer.",
		3 => "Your card has been declined. Please contact your card issuer.",
		4 => "Your card has been declined. Please contact your card issuer.",
		11 => "The same transaction was submitted twice. Please be patient. Please allow more than 2 mintues to pass and do not click the submit button more than once.",
		13 => "Please contact us at support@vieuu.com",
		14 => "Please contact us at support@vieuu.com",
		17 => "We do not accept the type of credit card you entered. We apologize for the inconvenience.",
		18 => "We do not accept the type of credit card you entered. We apologize for the inconvenience.",
		19 => "An error occurred with the credit card processor. You were not charged for this transaction. Please try again in 5 minutes.",
		20 => "An error occurred with the credit card processor. You were not charged for this transaction. Please try again in 5 minutes.",
		21 => "An error occurred with the credit card processor. You were not charged for this transaction. Please try again in 5 minutes.",
		22 => "An error occurred with the credit card processor. You were not charged for this transaction. Please try again in 5 minutes.",
		23 => "An error occurred with the credit card processor. You were not charged for this transaction. Please try again in 5 minutes.",
		25 => "An error occurred with the credit card processor. You were not charged for this transaction. Please try again in 5 minutes.",
		26 => "An error occurred with the credit card processor. You were not charged for this transaction. Please try again in 5 minutes.",
		27 => "The address entered does not match the billing address of the cardholder",
		28 => "We do not accept the type of credit card you previously entered. We apologize for the inconvenience.",
		193 => "Your transaction is being reviewed by our credit card processor. At this moment you were not charged for this transaction. However, we may process this transaction at a later time pending a response from the credit card processor.",
		252 => "Your transaction is being reviewed by our credit card processor. At this moment you were not charged for this transaction. However, we may process this transaction at a later time pending a response from the credit card processor.",
		253 => "Your transaction is being reviewed by our credit card processor. At this moment you were not charged for this transaction. However, we may process this transaction at a later time pending a response from the credit card processor.",
	];
}
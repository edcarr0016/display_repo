<?php namespace App\Payments\Exceptions;

use App\Payments\Exceptions\VendibleException;

class MissingQuantityException extends VendibleException {

	protected $message = "This %s does not have a quantity set.";

	public function __construct($designation, $code = 0, Exception $previous = null){
		$this->message = sprintf($this->message, $designation);
		parent::__construct($this->message, $code, $previous);
	}

}
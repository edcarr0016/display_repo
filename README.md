# Setup #
First and foremost you must have an Authorize.net account in order to use this implementation. They will provide you with log in credentials (Log In and Key). Those must be present in your .env file as AUTH_NET_LOG and AUTH_NET_KEY.

Once those have been obtained you must include the following lines in your config/app.php file:

Under the providers array include:

```
#!php

'App\Payments\ShoppingCartServiceProvider',
'App\Payments\CreditCardServiceProvider',
'App\Payments\PaytechServiceProvider',
```

Under the aliases array include:

```
#!php


'ShoppingCart'      => 'App\Payments\Facades\ShoppingCart',
'CreditCard'      => 'App\Payments\Facades\CreditCard',
'Paytech'      => 'App\Payments\Facades\Paytech',
```

# Implementation #

Whatever item it is you're planning to sell must implement the Vendible Interface. I created a VendibleTrait which will provide a lot of the necessary functionality for you, but there are a few methods which must be manually created by you. Once you have a Vendible object you add it to a cart like so:



```
#!php

$cart = ShoppingCart::add($vendible);
```

To process a payment you must use the App\User class provided by Laravel and the CreditCard class provided by this implementation which holds (you guessed it) the user's credit card information for payment processing. Once you have those two objects it's a simple matter of executing the following line:


```
#!php

Paytech::canProcessPurchase($user, $creditCard, $cart)
```
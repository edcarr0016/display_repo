<?php namespace App\Payments\Exceptions;

use App\Payments\Exceptions\VendibleException;

use Carbon\Carbon;

class OutsideSaleTimeException extends VendibleException {

	protected $messageBefore = "The sale time for this %s has expired (%s).";
	protected $messageAfter = "The sale time for this %s begins after %s";
	protected $messageBetween = "This %s is only for sale between %s and %s.";

	private $extremities = [1=>'minimum',2=>'maximum'];

	public function __construct($designation, Carbon $startTime, Carbon $endTime = null, $code = 0, Exception $previous = null){

		switch ($code) {
			case 1:
				$startString = $startTime->isToday() ? 'today starting at '.$startTime->format('g:ia') : $startTime->format('M jS g:ia');
				$endString = $endTime->isToday() ? $endTime->format('g:ia') : $endTime->format('M jS g:ia');

				$this->message = sprintf($this->messageBetween, $designation, $startString, $endString);
				break;
			
			case 2:
				$startString = $startTime->isToday() ? $startTime->format('g:ia') : $startTime->format('M jS g:ia');
				
				$this->message = sprintf($this->messageAfter, $designation, $startString);
				break;

			case 3:
				$endString = $endTime->isToday() ? $endTime->format('g:ia') : $endTime->format('M jS g:ia');
				
				$this->message = sprintf($this->messageBefore, $designation, $endString);
				break;
		}	

		parent::__construct($this->message, $code, $previous);
	}

}
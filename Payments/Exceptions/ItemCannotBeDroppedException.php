<?php namespace App\Payments\Exceptions;

use App\Payments\Exceptions\VendibleException;

class ItemCannotBeDroppedException extends VendibleException {

	protected $message = "This %s cannot be dropped because it has not been previously added; It does not exist in the shopping cart.";

	public function __construct($designation, $code = 0, Exception $previous = null){

		$this->message = sprintf($this->message, $designation);

		parent::__construct($this->message, $code, $previous);
	}
}
<?php namespace App\Payments\Exceptions;

use Exception;

class BlankValueException extends Exception {

const EXCEPTION_MESSAGE = "No value given for field: %s";

	public function __construct($field){
		parent::__construct(sprintf(self::EXCEPTION_MESSAGE, $field));
	}

}
<?php namespace App\Payments;

use Illuminate\Support\ServiceProvider;
use App\Payments\CreditCard;

class CreditCardServiceProvider extends ServiceProvider {

	/**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

	/**
	 * Register the application services.
	 *
	 * @return CreditCard
	 */
	public function register()
	{
		//
		$this->app->bind('creditcard', function(){
			return new CreditCard($this->app['session']);
		});
	}

	/** 
	 * This function is required by Laravel since the defer property 
	 *  is set to true. What happens is that this service is not 
	 *	loaded by the application until it is required.
	 */
	public function provides(){
		return ['creditcard'];
	}

}

<?php namespace App\Payments;

use Illuminate\Support\ServiceProvider;
use App\Payments\ShoppingCart;

class ShoppingCartServiceProvider extends ServiceProvider {

	/**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

	/**
	 * Register the application services.
	 *
	 * @return ShoppingCart
	 */
	public function register()
	{
		//
		$this->app->bind('shoppingcart', function(){
			return new ShoppingCart($this->app['session']);
		});

		/*$this->app->booting(function(){
	        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
	        $loader->alias('ShoppingCart', 'App\Payments\Facades\ShoppingCart');
	    });*/
	}

	/** 
	 * This function is required by Laravel since the defer property 
	 *  is set to true. What happens is that this service is not 
	 *	loaded by the application until it is required.
	 */
	public function provides(){
		return ['shoppingcart'];
	}
}

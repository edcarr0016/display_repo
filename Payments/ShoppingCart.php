<?php namespace App\Payments;

use Illuminate\Database\Eloquent\Model;
use App\Address;
use App\Event;
use App\Item;
use App\Offer;
use App\Venue;
use Carbon\Carbon;

use App\Payments\Contracts\Vendible;
use App\Payments\ShoppingCartCollection;

use App\Shipping\Shipment;
use App\Shipping\UPS;
use App\Shipping\USPS;

use App\Payments\Exceptions\OfferCannotBeAppliedException;
use App\Payments\Exceptions\OfferCannotBeRemovedException;

class ShoppingCart{

	/**
	 * Standard Session used by the app variable.
	 * @var Illuminate\Support\SessionManager
	 */
	private $session;

	/**
	 * Collection to manage the cart items.
	 * Easier to store and retrieve the items as a collection to and from the session.
	 * @var App\Payments\ShoppingCartCollection
	 */
	private $collection;


	private $checkoutTimeFilter;

	const LENGTH = 'length';
	const WIDTH = 'width';
	const HEIGHT = 'height';

	public function __construct($session){
		$this->session = $session;
		$this->collection = new ShoppingCartCollection();
	}

	public function add(Vendible $item){

		/**
		 * Aside from type hinting the item in the method, when the item 
		 * is inserted into the collection it is also validated
		 * to make sure that it is supported for purchase.
		 */
		
		$purchasable = $item->checkPurchasability();

		if($purchasable){//If item is not purchasable an exception will be thrown

			$this->collection->setItems($this->session->pull('shoppingcart',[]));

			$item->timeAddedToCart = Carbon::now();

			$items = $this->collection->insert($item);

			$this->session->put('shoppingcart', $items);
			// var_dump($this->session->pull('shoppingcart',[]));
			$this->collection->make($items);

			// return $this->collection->make($items);
			return $this;
			/**
			 * need to add logic to remove offers if a certain item is updated with quantity less than before.
			 */
		}
	}

	public function update(Vendible $item){
		$this->collection->setItems($this->session->pull('shoppingcart',[]));

		$items = $this->collection->update($item);

		$this->session->put('shoppingcart', $items);
		// var_dump($this->session->pull('shoppingcart',[]));
		$this->collection->make($items);

		// return $this->collection->make($items);
		return $this;
	}

	public function remove(Vendible $item){
		$this->collection->setItems($this->session->pull('shoppingcart',[]));

		$items = $this->collection->drop($item);

		$this->session->put('shoppingcart', $items);

		$this->collection->make($items);
		
		return $this;
	}

	public function applyOffer(Offer $offer){
		$this->collection->setItems($this->session->pull('shoppingcart',[]));
		
		try{
			$insurancePolicy = $this->collection->getItems();
			
			$items = $this->collection->apply($offer);
			
			$this->session->put('shoppingcart', $items);

			$this->collection->make($items);
		}catch(OfferCannotBeAppliedException $e){
			$this->session->put('shoppingcart', $insurancePolicy);
			throw $e;
		}

		return $this;
	}

	public function removeOffer(Offer $offer){
		$this->collection->setItems($this->session->pull('shoppingcart',[]));

		try{
			$insurancePolicy = $this->collection->getItems();

			$items = $this->collection->remove($offer);

			$this->session->put('shoppingcart', $items);

			$this->collection->make($items);
		}catch(OfferCannotBeRemovedException $e){
			$this->session->put('shoppingcart', $insurancePolicy);
			throw $e;
		}
		
		return $this;
	}

	/**
	 * Will go to the session and get the cart with all it's items for the user
	 * @return Collection Items, EventPrices, EventSpecials
	 */
	public function getAllItems($applyCheckoutFilter = false){
		$items = $this->collection->make($this->session->get('shoppingcart'));

		return $items->filter(function($item) use ($applyCheckoutFilter){
			switch ($applyCheckoutFilter) {
				case true:
						if(is_null($this->checkoutTimeFilter)){
							$this->checkoutTimeFilter = new Carbon('now');
						}

						if($item->timeAddedToCart < $this->checkoutTimeFilter){
							return $item;
						}
					break;
				
				default:
					return $item;
					break;
			}
		});
	}

	/**
	 * Returns an instance of the ShoppingCart class.
	 * @return ShoppingCart convenience method intended to provide an instance of the Shopping Cart
	 */
	public function getInstance(){
		return $this;
	}

	/**
	 * Clears all items from the session.
	 * @return ShoppingCart empty cart
	 */
	public function clear(){
		$fullCart = $this->getAllItems();
		$filtered = $this->getAllItems(true);
		$remaining = $fullCart->diff($filtered);

		$this->collection = new ShoppingCartCollection();
		$this->session->forget('shoppingcart');
    $this->session->forget('shoppingcart-shipment');
		$this->checkoutTimeFilter = null;

		foreach ($remaining as $item) {
			$this->add($item);
		}

		return $this;
	}

	public function setShipment(Shipment $shipment){
		$this->session->put('shoppingcart-shipment', $shipment);
	}

	public function getShipment(){
		return $this->session->get('shoppingcart-shipment');
	}

	public function getShipmentPrice(){
		$shipment = $this->getShipment();
		return !is_null($shipment) ? $shipment->price : 0;
	}

	public function setCheckoutTime(){
		$this->checkoutTimeFilter = new Carbon('now');
	}

	/**
	 * Will sum the service fee property on all cart items
	 * @param  boolean $format added a switch to return formatted number for display purposes
	 * @return Number (float)
	 */
	public function getTotalServiceFee($format = false){
		$items = $this->getAllItems(true);

		$totalServiceFee = $items->sum(function($item){
			return $item->getFeeTotal();
		});

		return $format ? number_format($totalServiceFee, 2, '.', ',') : $totalServiceFee;
	}

	/**
	 * Will sum the tax property on all cart items
	 * @param  boolean $format added a switch to return formatted number for display purposes
	 * @return Number (float)
	 */
	public function getTotalTax($format = false){
		$items = $this->getAllItems(true);

		$totalTax = $items->sum(function($item){
			return $item->getTaxTotal();
		});

		return $format ? number_format($totalTax, 2, '.', ',') : $totalTax;
	}

	/**
	 * Will sum the price property on all cart items
	 * @param  boolean $format added a switch to return a formatted number for display purposes
	 * @return Number (float)
	 */
	public function getTotalPrice($format = false){
		$items = $this->getAllItems(true);

		$totalPrice = $items->sum(function($item){
			return $item->getPriceTotal();
		});

		return $format ? number_format($totalPrice, 2, '.', ',') : $totalPrice;
	}

	public function getSubtotal($format = false){
		$items = $this->getAllItems(true);

		$subtotal = $items->sum(function($item){
			return $item->getSubtotal();
		});

		$subtotal += $this->getShipmentPrice();
		
		return $format ? number_format($subtotal, 2, '.', ',') : $subtotal;
	}

	/**
	 * Will sum the service fees, taxes, and prices
	 * @param  boolean $format added a switch to return a formatted number for display purposes
	 * @return Number (float)
	 */
	public function getTotal($format = false){
		$items = $this->getAllItems(true);

		$total = $items->sum(function($item){
			return $item->getTotal();
		});

		$total += $this->getShipmentPrice();

		return $format ? number_format($total, 2, '.', ',') : $total;
	}

	public function groupItems(){
		$items = $this->getAllItems(true);
		$items->transform(function($item){
			$item->subTotal = $item->getSubtotal(true);
			$item->total = $item->getTotal(true);
			$item->dbDesignation = $item->getDbEnum();
			return $item;
		});

		$events = [];
		$eventItems = [];
		$modelItems = [];

		foreach ($items as $item) {
			if($item->event){
				$event = $item->event;
				unset($item->event);
				$eventItems[$event->id][] = $item;
				if(!in_array($event, $events)){
					array_unshift($events, $event);
				}
			}else{
				array_unshift($modelItems, $item);
			}
		}

		foreach ($events as &$event) {
			$event->items = $eventItems[$event->id];
		}

		empty($modelItems) ? : array_unshift($events, $modelItems);

		return $items->make($events);
	}

	public function getDescription(){

		//Get all items in the shopping cart
		//Get all the Event Names
		//Also get all the venues, if there is no event name
		
		//If there is less than one event name, then use that event name.
		//	Else, there is more than one event, say "multiple occassions for" and implode venue names 

		$items = $this->getAllItems(true);

		$description = '';
		$occassionItemDescriptions = [];
		$itemDescriptions = [];

		$eventNames = [];
		$venueNames = [];

		foreach ($items as $item) {
			if($item->event){
				$event = $item->event->name;
				if(!in_array($event, $eventNames)){
					$eventNames[] = $event;
					$venueNames[] = $item->event->venue->name.' '.$item->event->venue->types->first()->name;
				}
			}else if($item->venue){
				$venue = $item->venue->name.' '.$item->venue->types->first()->name;
				if(!in_array($venue, $venueNames)){
					$venueNames[] = $venue;
				}
			}
			
			if($item->event || $item->venue){
				$occassionItemDescriptions[] = $item->getDesignation(true);
			}else{
				$itemDescriptions[] = $item->getDesignation(true);
			}
		}

		$descriptionCount = count($occassionItemDescriptions);
		if($descriptionCount > 1){
			$occassionItemDescriptions[$descriptionCount - 1] = 'and '.$occassionItemDescriptions[$descriptionCount - 1]; 
		}

		$itemDescriptionCount = count($itemDescriptions);
		if($itemDescriptionCount > 1){
			$itemDescriptions[$itemDescriptionCount - 1] = 'and '.$itemDescriptions[$itemDescriptionCount - 1];
		}

		$venueCount = count($venueNames);
		
		$description .= $descriptionCount > 2 ? implode(', ', $occassionItemDescriptions) : implode(' ', $occassionItemDescriptions);
		
		if(count($eventNames) <= 1){
			$description .= ' for '.$eventNames[0].' at '.$venueNames[0];
		}else{
			if($venueCount > 1){
				$venueNames[$venueCount - 1] = 'and '.$venueNames[$venueCount - 1];
			}
			
			$description .= ' for ';
			$description .= $venueCount > 2 ? implode(', ', $venueNames) : implode(' ', $venueNames);
		}

		if(!empty($itemDescriptions)){
			$description .= '; ';
			$description .= $itemDescriptionCount > 2 ? implode(', ', $itemDescriptions) : implode(' ', $itemDescriptions);
		}

		return $description;
	}

	/**
	 * Will sum the weight of all Items in the cart
	 * @return float total weight of all Items in the cart
	 */
	public function getTotalWeight(){
		$items = $this->getAllItems(true);

		$totalWeight = $items->sum(function($item){
			if($item instanceof Item){
				return $item->quantity*$item->unit_weight;
			}
		});

		return $totalWeight;
	}

	/**
	 * Will sum the volume for all Items in the cart
	 * @return Array returns the total length, width, and height of all Items in the cart
	 */
	public function getTotalVolume(){
		$items = $this->getAllItems(true);

		$totalVolume = $items->sum(function($item){
			/**
			 * Calculating the volume would assume that the items will be occupying every cubed inch of space
			 * within the container. In the real world things will not fit together so perfectly. So I am
			 * adding some additional space so that Items can be comfortably packaged.
			 * @var float
			 */
			$spacer = 0.5;

			if($item instanceof Item){
				$unit_volume = $item->unit_length
								*$item->unit_width
								*$item->unit_height
								+ pow($spacer, 3);
				return $item->quantity*$unit_volume;
			}
		});

		return $totalVolume;
	}

	/**
	 * Helper function used to determine the dimensions necessary to deliver
	 * the items being bought by a user.
	 * 
	 * @param  ShoppingCart $cart Cart should have the items which will be bought.
	 * @return Array             Dimension array containing estimated length, width, and height of package.
	 */
	public function getPackageDimensions(){
		/**
		 * Rudimentary implementation to determine the size of the package that will be mailed.
		 * At the bottom of the file I have the algorithm I would like to apply to get more 
		 * accurate measurements for the package to be delivered. Research would be
		 * into the "Bin Packing Problem". Would not completely utilize Bin
		 * Packing formula. My implementation would still only approcimate
		 * but would be better than this implementation.
		 */
		
		$greatestLength = $this->getAllItems(true)->sortBy('unit_'.self::LENGTH)->last()->unit_length;
		$greatestWidth = $this->getAllItems(true)->sortBy('unit_'.self::WIDTH)->last()->unit_width;
		$greatestHeight = $this->getAllItems(true)->sortBy('unit_'.self::HEIGHT)->last()->unit_height;

		/* Determine which is greater, Width or Length*/
		$greatestWidthLength = $greatestWidth > $greatestLength ? $greatestWidth : $greatestLength;

		$totalVolume = $this->getTotalVolume();

		/**
		 * Get the surface area if we were to use a single layer box. All Items would be placed
		 * on a single plane with varying heights. This is where a large amount of the
		 * inaccuracy is contained as there would be free space which could be used 
		 * by smaller items if they were to be stacked. The algorithm will
		 * attempt to achieve this.
		 */
		$totalSurfaceArea = $totalVolume/$greatestHeight;

		/* Use the next greatest dimension to determine the size of the remaining side.*/
		$remainingSide = $totalSurfaceArea/$greatestWidthLength;

		$dimensions = [
			self::LENGTH => $greatestWidthLength,
			self::WIDTH  => $remainingSide,
			self::HEIGHT => $greatestHeight
			];

		return $dimensions;
	}

	/**
	 * Step 1: Compute the Total Surface area for all Items
	 * Step 2: If an un-even number, figure out which surface area would fit best to remaining space and subtract from total packages
	 * Step 2a: put largest height in separate variable
	 * Step 3: Once complete, repeat process for next level.
	 * Step 4: Sum heights, determine if any side is greater than 12.
	 */
}
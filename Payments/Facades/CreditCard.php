<?php namespace App\Payments\Facades;

use Illuminate\Support\Facades\Facade;

class CreditCard extends Facade{

	protected static function getFacadeAccessor(){return 'creditcard'; }
}

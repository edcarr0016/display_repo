<?php namespace App\Payments\Exceptions;

use Carbon\Carbon;
use Exception;

abstract class VendibleException extends Exception {

	public function __construct($message = null, $code = 0, Exception $previous = null){

		parent::__construct($message, $code, $previous);

		$baseDir = getenv('BASE_DIR');
		$logDir = $this->removeFirstVar('LOG_DIR');
		$exceptionsDir = $this->removeFirstVar('EXCEPTION_ANALYSIS');

		$now = Carbon::parse('now');

		$dir = $baseDir.'/'.$logDir.'/'.$exceptionsDir.'/vendible';
		$filename = $dir.'/'.$now->toDateString().'.log';
		$contents = "\n".$now->toDateTimeString()."		".$this->getMessage()."\n";
		$contents .= $this->getTraceAsString();

		if(!file_exists($dir)){
			mkdir($dir, '0777', true);
		}

		file_put_contents($filename, $contents, FILE_APPEND);
	}

	private function removeFirstVar($envVar){

		$dir = getenv($envVar);
		$dirParts = explode('/', $dir);
		array_shift($dirParts);

		return implode('/', $dirParts);
	}

}
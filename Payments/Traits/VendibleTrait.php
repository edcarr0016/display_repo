<?php namespace App\Payments\Traits;

use App\Offer;
use Carbon\Carbon;

use App\Payments\Exceptions\OfferAppliedException;
use App\Payments\Exceptions\OfferCannotBeAppliedException;
use App\Payments\Exceptions\OfferCannotBeRemovedException;
use App\Payments\Exceptions\NotForSaleException;
use App\Payments\Exceptions\OutsideSaleTimeException;
use App\Payments\Exceptions\PurchaseQuantityException;
use App\Payments\Exceptions\MissingQuantityException;

trait VendibleTrait {

	protected $offers = [];

	protected $freeQuantity = 0;

	public function checkPurchasability(Carbon $now = null){
		$this->checkForQuantity();

		if(!is_null($this->purchasable) && !$this->purchasable){
			throw new NotForSaleException($this->getDesignation(), 2);
		}

		if(!is_null($this->min_purch_quantity) && $this->quantity < $this->min_purch_quantity){
			throw new PurchaseQuantityException($this->getDesignation(),$this->min_purch_quantity,1);
		}

		if(!is_null($this->max_purch_quantity) && $this->quantity > $this->max_purch_quantity){
			throw new PurchaseQuantityException($this->getDesignation(),$this->max_purch_quantity,2);
		}

		if(is_null($now)){
			$now = new Carbon('now');
		}

		if(!is_null($this->start_time) & !is_null($this->end_time)){
			if(!($now->between($this->start_time, $this->end_time))){
				throw new OutsideSaleTimeException($this->getDesignation(),$this->start_time,$this->end_time, 1);
			}
		}else if(!is_null($this->start_time) & is_null($this->end_time)){
			if($now->lt($this->start_time)){
				throw new OutsideSaleTimeException($this->getDesignation(),$this->start_time, null, 2);
			}
		}else if(is_null($this->start_time) & !is_null($this->end_time)){
			if($now->gt($this->end_time)){
				throw new OutsideSaleTimeException($this->getDesignation(),$now, $this->end_time, 3);
			}
		}

		if($this->isSoldOut()){
			throw new NotForSaleException($this->getDesignation(), 1);
		}

		return true;
	}

	public function isSoldOut(){
		return $this->sold_out;
	}

	public function getFeeTotal($formatNumber = false){
		$this->checkForQuantity();

		$feeTotal = $this->serv_fee*($this->quantity-$this->freeQuantity);

		return $formatNumber ? number_format($feeTotal, 2, '.', ',') : $feeTotal;
	}

	public function getPriceTotal($formatNumber = false){
		$this->checkForQuantity();

		$priceTotal = $this->price*($this->quantity-$this->freeQuantity);

		return $formatNumber ? number_format($priceTotal, 2, '.', ',') : $priceTotal;
	}

	public function getSubtotal($formatNumber = false){
		$this->checkForQuantity();

		$subtotal = ($this->price+$this->serv_fee)*($this->quantity-$this->freeQuantity);

		return $formatNumber ? number_format($subtotal, 2, '.', ',') : $subtotal;
	}

	public function getTotal($formatNumber = false){
		$total = $this->getSubtotal()+$this->getTaxTotal();

		return $formatNumber ? number_format($total, 2, '.', ',') : $total;
	}

	public function applyOffer(Offer $offer){
		//Offer can reduce
		//	Price
		//	Percent of Price
		//	Or Quantity
			
		$this->checkForQuantity();

		if(strcasecmp($offer->for_type, $this->getDbEnum()) != 0 || $offer->for_id != $this->id){
			throw new OfferCannotBeAppliedException($this->getDesignation());
		}

		if(in_array($offer, $this->offers)){
			throw new OfferAppliedException($this->getDesignation());
		}

		if(!is_null($offer->price)){
			$this->price -= $offer->price;
			$this->offers[] = $offer;
		}else if(!is_null($offer->percent)){
			$this->price -= $this->find($this->id)->price*$offer->percent;
			$this->offers[] = $offer;
		}else if(!is_null($offer->quantity)){
			$this->freeQuantity += $offer->quantity;
			$this->offers[] = $offer;
		}

		return $this;
	}

	public function removeOffer(Offer $offer){
		$this->checkForQuantity();

		if(!in_array($offer, $this->offers)){
			throw new OfferCannotBeRemovedException($this->getDesignation());
		}

		if(!is_null($offer->price)){
			$this->price += $offer->price;
			unset($this->offers[array_search($offer, $this->offers)]);
		}else if(!is_null($offer->percent)){
			$this->price += $this->find($this->id)->price*$offer->percent;
			unset($this->offers[array_search($offer, $this->offers)]);
		}else if(!is_null($offer->quantity)){
			$this->freeQuantity -= $offer->quantity;
			unset($this->offers[array_search($offer, $this->offers)]);
		}

		return $this;
	}

	public function getAppliedOffers(){
		return $this->offers;
	}

	protected function checkForQuantity(){
		if(is_null($this->quantity)){
			throw new MissingQuantityException($this->getDesignation());
		}
	}
}
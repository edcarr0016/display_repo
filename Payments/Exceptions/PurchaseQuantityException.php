<?php namespace App\Payments\Exceptions;

use App\Payments\Exceptions\VendibleException;

class PurchaseQuantityException extends VendibleException {

	protected $message = "The %s purchase quantity for this %s is %s";

	private $extremities = [1=>'minimum',2=>'maximum'];

	public function __construct($designation, $purchaseQuantity, $code = 1, Exception $previous = null){
		$this->message = sprintf($this->message, $this->extremities[$code], $designation, $purchaseQuantity);
		parent::__construct($this->message, $code, $previous);
	}

}
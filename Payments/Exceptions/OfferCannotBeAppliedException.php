<?php namespace App\Payments\Exceptions;

use App\Payments\Exceptions\VendibleException;

class OfferCannotBeAppliedException extends VendibleException {

	protected $message = "This offer is not applicable for this %s.";

	public function __construct($designation, $code = 0, Exception $previous = null){

		$this->message = sprintf($this->message, $designation);
			
		parent::__construct($this->message, $code, $previous);
	}

}
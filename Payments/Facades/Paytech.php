<?php namespace App\Payments\Facades;

use Illuminate\Support\Facades\Facade;

class Paytech extends Facade{

	protected static function getFacadeAccessor(){return 'paytech'; }
}

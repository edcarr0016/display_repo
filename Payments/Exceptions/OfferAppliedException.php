<?php namespace App\Payments\Exceptions;

use App\Payments\Exceptions\VendibleException;

class OfferAppliedException extends VendibleException {

	protected $message = "This offer has already been applied to this %s.";

	public function __construct($designation, $code = 0, Exception $previous = null){

		$this->message = sprintf($this->message, $this->getDesignation());
			
		parent::__construct($this->message, $code, $previous);
	}

}
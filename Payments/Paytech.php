<?php namespace App\Payments;

use App;
use App\User;
use App\Payments\CreditCard;
use App\Payments\ShoppingCart;

use App\Payments\Exceptions\UnableToChargeException;
use App\Payments\Exceptions\CardDeclinedException;
use App\Payments\Exceptions\ErrorProcessingTransactionException;
use App\Payments\Exceptions\TransactionBeingHeldForReviewException;

/*
	$post_url = "https://test.authorize.net/gateway/transact.dll";

	$post_values = array(
		
		// the API Login ID and Transaction Key must be replaced with valid values
		"x_login"			=> "API_LOGIN_ID",
		"x_tran_key"		=> "TRANSACTION_KEY",

		"x_version"			=> "3.1",
		"x_delim_data"		=> "TRUE",
		"x_delim_char"		=> "|",
		"x_relay_response"	=> "FALSE",

		"x_type"			=> "AUTH_CAPTURE",
		"x_method"			=> "CC",
		"x_card_num"		=> "4111111111111111",
		"x_exp_date"		=> "0115",

		"x_amount"			=> "19.99",
		"x_description"		=> "Sample Transaction",

		"x_first_name"		=> "John",
		"x_last_name"		=> "Doe",
		"x_address"			=> "1234 Street",
		"x_state"			=> "WA",
		"x_zip"				=> "98004"
		// Additional fields can be added here as outlined in the AIM integration
		// guide at: http://developer.authorize.net
	);

 */

class Paytech{

	//Production URL, will hit the actual live Authorize.net server.
	//Live server has the ability to accept x_test_request flag to test responses for Actual Client Data
	private $post_url = "https://secure.authorize.net/gateway/transact.dll";

	//Test URL, will hit a test environment where stubbed responses can be obtained.
	private $dev_post_url = "https://test.authorize.net/gateway/transact.dll";

	//Based on environment config, I want to determine which post URL I shoul dbe using
	private $environment;

	private $post_values = array(
		// the API Login ID and Transaction Key must be replaced with valid values
		"x_login"			=> null,
		"x_tran_key"		=> null,

		"x_version"			=> "3.1",
		"x_delim_data"		=> "TRUE",
		"x_delim_char"		=> "|",
		"x_relay_response"	=> "FALSE",

		"x_type"			=> "AUTH_CAPTURE",
		"x_method"			=> "CC",
		"x_card_num"		=> null,
		"x_card_code"		=> null, 
		"x_exp_date"		=> null,

		"x_tax"				=> null,
		"x_amount"			=> null,
		"x_description"		=> null,

		"x_first_name"		=> null,
		"x_last_name"		=> null,
		"x_address"			=> null,
		"x_state"			=> null,
		"x_zip"				=> null,

		"x_email"			=> null
		// Additional fields can be added here as outlined in the AIM integration
		// guide at: http://developer.authorize.net
	);

	/*
		Paytech Controller can receive a request to validate credit card information.

		When the class is initalized it will determine if it is in production or the test environment.

		It will send a request to Paymentech with the information it was provided.

	 */
	
	const DEV = 'Development';
	const PROD = 'Production';
	
	public function __construct(){
		$this->environment = App::detectEnvironment(function(){
			 if(strcasecmp(Paytech::PROD, getenv('APP_ENV')) == 0){
			 	return Paytech::PROD; 
			 }else{
			 	return Paytech::DEV;
			 }
		});

		$this->post_values['x_login'] = App::detectEnvironment(function(){
			return getenv('AUTH_NET_LOG');
		});

		$this->post_values['x_tran_key'] = App::detectEnvironment(function(){
			return getenv('AUTH_NET_KEY');
		});
	}
	
	public function canProcessPurchase(User $user, CreditCard $creditCard, ShoppingCart $cart, $prod_simulation = false){
		$this->setUserData($user, $creditCard);
		$this->setCardData($cart);
		$this->setEndpoint($prod_simulation);

		$post_string = "";

		foreach($this->post_values as $key => $value){
			$post_string .= "$key=" . urlencode( $value ) . "&";
		}
		$post_string = rtrim( $post_string, "& " );
		
		
		$request = curl_init($this->post_url); // initiate curl object
		curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
		curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
		curl_setopt($request, CURLOPT_POSTFIELDS, $post_string); // use HTTP POST to send form data
		curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line if you get no gateway response.
		$post_response = curl_exec($request); // execute curl post and store results in $post_response
		// additional options may be required depending upon your server configuration
		// you can find documentation on curl options at http://www.php.net/curl_setopt
		curl_close ($request);
		
		$response_array = explode($this->post_values["x_delim_char"],$post_response);

		return $this->decipherResponse($response_array);
	}

	private function setUserData(User $user, CreditCard $creditCard){
		//populate Authorize post values with the name of the person who is purchasing.
		//If the name on the credit card is different from that of the user,
		//	use the credit card name instead
		if((strcasecmp($creditCard->getFirstName(),$user->first_name) != 0) || 
			(strcasecmp($creditCard->getLastName(),$user->last_name) != 0)){
			$this->post_values['x_first_name'] = $creditCard->getFirstName();
			$this->post_values['x_last_name'] = $creditCard->getLastName();
		}else{
			$this->post_values['x_first_name'] = $user->first_name;
			$this->post_values['x_last_name'] = $user->last_name;
		}

		// $this->post_values['x_email'] = $user->email;

		//populate post values for Address
		$this->post_values['x_address'] = $creditCard->getAddress();
		$this->post_values['x_state'] = $creditCard->getState();
		$this->post_values['x_zip'] = $creditCard->getZip();

		//populate actual card information
		$this->post_values['x_card_num'] = $creditCard->getNumber();
		$this->post_values['x_card_code'] = $creditCard->getCode();
		$this->post_values['x_exp_date'] = $creditCard->getExpirationDate();
	}

	private function setCardData(ShoppingCart $cart){
		//populate tax, amount and description
		
		/**
		 * Delimiter <|>
		 * Tax data structure should be TaxItemName<|>Tax Description<|>TaxAmount
		 */
		$taxName = "Tax";
		$taxDesc = "City/State/Region Tax";
		$taxAmount = $cart->getTotalTax(true);//Boolean flag to enable number formating
		$tax = implode('<|>', [$taxName, $taxDesc, $taxAmount]);
		$this->post_values['x_tax'] = $tax;
		$this->post_values['x_amount'] = $cart->getTotal(true);
		$this->post_values['x_description'] = $cart->getDescription();
	}

	private function setEndpoint($prod_simulation){
		switch ($this->environment) {
			case self::DEV:
				if($prod_simulation){
					$this->post_values["x_test_request"] = "TRUE";
				}else{
					$this->post_url = $this->dev_post_url;
					$this->post_values["x_login"] = "8R8ea7NW";
					$this->post_values["x_tran_key"] = "5WkmZZ966u278X2v";
					$this->post_values["x_card_num"] = "4007000000027";
					$this->post_values["x_exp_date"] = "1220";
				}
				break;
			
			default:
				/**
				 * Default mode will be self::PROD.
				 * Code will use live production settings even if the $prod_simulation
				 * flag is set to TRUE.
				 */
				break;
		}
	}

	private function decipherResponse($response_array){
		/**
		 * Response Array[
		 * 	0 => [
		 * 			Explanation => Response from Authorize.net. Indicates the overall status of the transaction,
		 * 			Possible Values => [1=>success, 2=>declined, 3=>error, 4=>held for review]
		 * 		]
		 * 	2 => Response Reason Code
		 * 	3 => Response Reason Text
		 * ]
		 */
		
		$code = $response_array[0];
		$reasonCode = $response_array[2];
		$reasonText = $response_array[3];

		switch ($response_array[0]) {
			case 1:
				return true;
				break;
			
			case 2:
				throw new CardDeclinedException($reasonCode, $reasonText);
				break;

			case 3:
				throw new ErrorProcessingTransactionException($reasonCode, $reasonText);
				break;

			case 4:
				throw new TransactionBeingHeldForReviewException($reasonCode, $reasonText);
				break;

			default:
				throw new UnableToChargeException();
				break;
		}
	}

}
<?php namespace App\Payments;

use Illuminate\Support\Collection as IlluminateCollection;

use App\Item;
use App\Offer;

use App\Payments\Contracts\Vendible;

use App\Payments\Exceptions\ItemCannotBeDroppedException;
use App\Payments\Exceptions\OfferAppliedException;
use App\Payments\Exceptions\OfferCannotBeAppliedException;
use App\Payments\Exceptions\OfferCannotBeRemovedException;

class ShoppingCartCollection extends IlluminateCollection{

	//All Added Items
	protected $items;

	/**
	 * Convenience method provided to easily add all items. To be used 
	 * to store all items returned by the session.
	 * @param array $items All items in a User's cart.
	 */
	public function setItems(array $items){

		$this->items = $items;
	}

	public function getItems(){
		return $this->items;
	}

	/**
	 * With provided Item first will check that it is an item that is supported.
	 * Then will check if the array of Items already contains Item.
	 * 	If true it will replace the item in the array.
	 * 	Else it will add the item to the array.
	 * @param  EventPrices|EventSpecials|Item|Offer $item The Item in the user's shopping cart
	 * @return Array<@param>       Array of EventPrices|EventSpecials|Item|Offer
	 */
	public function insert(Vendible $item){

		if(($key = $this->alreadyContains($item)) === false){
			$this->items[] = $item;
		}else{
			$this->items[$key]->quantity += $item->quantity;
		}

		return $this->items;
	}

	public function update(Vendible $item){

		if(($key = $this->alreadyContains($item)) === false){
			$this->items[] = $item;
		}else{
			$this->items[$key] = $item;
		}

		return $this->items;
	}

	public function drop(Vendible $item){

		if(($key = $this->alreadyContains($item)) === false){
			throw new ItemCannotBeDroppedException($item->getDesignation());
		}else{
			unset($this->items[$key]);
		}

		return $this->items;
	}

	public function apply(Offer $offer){
		$numOfItems = count($this->items);

		if(empty($numOfItems)){
			throw new OfferCannotBeAppliedException("cart because it is empty");
		}

		foreach ($this->items as $key => &$oneItem) {
			try{
				$item = $oneItem->applyOffer($offer);
				if(!is_null($item)){
					break;
				}
			}catch(OfferCannotBeAppliedException $e){
				if($numOfItems == $key+1){
					throw new OfferCannotBeAppliedException("cart");
				}
			}
		}

		return $this->items;
	}


	public function remove(Offer $offer){
		$numOfItems = count($this->items);

		if(empty($numOfItems)){
			throw new OfferCannotBeRemovedException("cart because it is empty");
		}

		foreach ($this->items as $key => &$oneItem) {
			try{
				$item = $oneItem->removeOffer($offer);
				if(!is_null($item)){
					break;
				}
			}catch(OfferCannotBeRemovedException $e){
				if($numOfItems == $key+1){
					throw new OfferCannotBeRemovedException("cart");
				}
			}
		}

		return $this->items;
	}

	/** 
	 * Need a function that will scan the array of items and determine if the type of item with the same ID
	 * already exists in the array. If so then would need to return a boolean value true/false.
	 */
	public function alreadyContains(Vendible $item){

		foreach ($this->items as $key => $oneItem) {
			$targetClass = get_class($oneItem);

			if(strcasecmp($targetClass, get_class($item)) == 0 && $item->id == $oneItem->id){
				return $key;
			}
		}

		return false;
	}
}
<?php namespace App\Payments\Exceptions;

class UnableToChargeException extends PaytechException {

	protected $message = 'Our credit card processor was unable to charge your card. Please verify your payment information is correct and try again.';

	public function __construct(){
		parent::__construct($this->message);
	}

}
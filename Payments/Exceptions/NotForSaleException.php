<?php namespace App\Payments\Exceptions;

use App\Payments\Exceptions\VendibleException;

class NotForSaleException extends VendibleException {

	protected $message = "This %s is %s";

	public function __construct($designation, $code = 0, Exception $previous = null){
		switch ($code) {
			case 1://Sold Out
				$this->message = sprintf($this->message, $designation, 'sold out.');
				break;
			
			case 2://not for sale
				$this->message = sprintf($this->message, $designation, 'not for sale.');
				break;
		}

		parent::__construct($this->message, $code, $previous);
	}

}
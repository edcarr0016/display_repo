<?php namespace App\Payments;

use Illuminate\Support\ServiceProvider;
use App\Payments\Paytech;

class PaytechServiceProvider extends ServiceProvider {

	/**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

	/**
	 * Register the application services.
	 *
	 * @return Paytech
	 */
	public function register()
	{
		//
		$this->app->bind('paytech', function(){
			return new Paytech($this->app['session']);
		});
/*
		$this->app['shoppingcart'] = $this->app->share(function($app)
		{
			$session = $app['session'];
			return new Paytech($session);
		});*/
	}

	/** 
	 * This function is required by Laravel since the defer property 
	 *  is set to true. What happens is that this service is not 
	 *	loaded by the application until it is required.
	 */
	public function provides(){
		return ['paytech'];
	}

}

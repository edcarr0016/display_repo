<?php namespace App\Payments\Contracts;

use Carbon\Carbon;

use App\Offer;

interface Vendible{

	/**
	 * Supposed to provide a small description of the item being sold.
	 * @return String
	 */
	public function getDescription();

	/**
	 * Supposed to provide the type of item which is being purchased.
	 * @return String
	 */
	public function getDesignation($formatForCart = false);

	/**
	 * Return the 2 letter database enumeration value used for vendible object in Transaction Table
	 * @return String
	 */
	public function getDbEnum();

	/**
	 * Determines if an item is purchasable or not. Should take into account
	 * the purchasable flag as well as the time limit on the item
	 * @return boolean
	 */
	public function checkPurchasability(Carbon $now = null);

	/**
	 * Determines wether the sold out flag is set, or if the item is beyond
	 * the maximum amount of sales.
	 * @return boolean [description]
	 */
	public function isSoldOut();

	/**
	 * Determines how to obtain the tax for the item and returns the tax amount that should be charged.
	 * @param  boolean $formatNumber Boolean flag to determine if should be send back with 2 decimal points
	 * @return float 
	 */
	public function getTaxTotal($formatNumber = false);

	/**
	 * The product of the service fee and quantity
	 * @param  boolean $formatNumber Flag to determine if should be sent back with 2 decimal points
	 * @return float
	 */
	public function getFeeTotal($formatNumber = false);

	/**
	 * The product of the price and quantity
	 * @param  boolean $formatNumber [description]
	 * @return [type]                [description]
	 */
	public function getPriceTotal($formatNumber = false);

	/**
	 * Meant to product of the quantities being purchased and the price of the item
	 * @param  boolean $formatNumber determine if should be send back with 2 decimal points
	 * @return float                
	 */
	public function getSubtotal($formatNumber = false);

	/**
	 * Should return the sum of tax and subtotal
	 * @param  boolean $formatNumber determine if should be send back with 2 decimal points
	 * @return float
	 */
	public function getTotal($formatNumber = false);

	/**
	 * Should apply the given offer to the item.
	 * Reduce by quantity, price, or percentage.
	 * @param  Offer  $offer 
	 * @return null
	 */
	public function applyOffer(Offer $offer);

	/**
	 * Should remove an offer which has already been applied
	 * and reverse it's effects.
	 * @param  Offer  $offer
	 * @return null
	 */
	public function removeOffer(Offer $offer);
}
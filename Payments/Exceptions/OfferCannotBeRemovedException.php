<?php namespace App\Payments\Exceptions;

use App\Payments\Exceptions\VendibleException;

class OfferCannotBeRemovedException extends VendibleException {

	protected $message = "This offer was never applied to this %s, it cannot be removed.";

	public function __construct($designation, $code = 0, Exception $previous = null){
	
		$this->message = sprintf($this->message, $designation);

		parent::__construct($this->message, $code, $previous);
	}

}
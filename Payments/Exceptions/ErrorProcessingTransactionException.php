<?php namespace App\Payments\Exceptions;

class ErrorProcessingTransactionException extends PaytechException {

	public function __construct($reasonCode, $reasonText){
		if(array_key_exists($reasonCode, $this->modedErrorMsgs)){
			$reasonText = $this->modedErrorMsgs[$reasonCode];
		}
		parent::__construct($reasonText, $reasonCode);
	}

}
<?php namespace App\Payments\Exceptions;

use Exception;

class CreditCardNumberLengthException extends Exception {

const EXCEPTION_MESSAGE = "The credit card number must be 16 digits long";

	public function __construct(){
		parent::__construct(self::EXCEPTION_MESSAGE);
	}

}